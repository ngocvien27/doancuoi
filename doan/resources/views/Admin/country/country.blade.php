@extends('Admin.layouts.app')
@section("content") 
     <?php
            $html = '';
            foreach ($data as $value) {
                $html .= '
                <tr role="row">
                    <td>'.$value['id'].'</td>
                    <td>'.$value['name'].'</td>
                    <td><a href="'.url('/country/delete/'.$value['id']).'"> Delete</a></td>
                </tr>';
            }
    ?>
    <table style="border: 1px solid">
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Country</th>
                    <th style="width: 10%;">Delete</th>
                </tr>
            </thead>
            <tbody>
                <?php  
                    echo $html;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <div class="col-sm-12">
                                <a href="{{ url('/country/add')}}"> <button name="submit" type="submit" class="btn btn-success">ADD Country</button></a>
                        </div>
                    </td>
                </tr>
            </tfoot>
    </table>
@endsection
