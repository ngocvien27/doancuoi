@extends('Admin.layouts.app')
@section("content") 
    <form action="" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <th>Country</th></br>  
        <input type="text" name="name" value="">
        <div class="col-sm-12">
            <button name="submit" type="submit" class="btn btn-success">Create Country</button>
        </div>
    </form>
    @if($errors->any())
        <ul>
           @foreach($errors->all() as $error)
                <li>{{$error}}</li>
                    @endforeach
        </ul>
    @endif
@endsection


