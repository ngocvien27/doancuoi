@extends('Admin.layouts.app')
@section("content") 
    <?php
            $html = '';
            foreach ($data as $value) {
                $html .= '
                <tr role="row">
                    <td>'.$value['id'].'</td>
                    <td>'.$value['title'].'</td>
                    <td>'.$value['image'].'</td>
                    <td>'.$value['description'].'</td>
                    <td><a href="'.url('admin/blog/edit/'.$value['id']).'"> Edit</a></td>
                    <td><a href="'.url('admin/blog/delete/'.$value['id']).'"> Delete</a></td>
                </tr>';
            }
    ?>
    <table style="border: 1px solid,width=100%">
             <thead>
                <tr role="row">
                    <th style="width: 20%;">ID</th>
                    <th style="width: 20%;">Title</th>
                    <th style="width: 15%;">Image</th>
                    <th style="width: 40%;">Description</th>
                    <th style="width: 7%;">Edit</th>
                    <th style="width: 10%;">Delete</th>
                </tr>
            </thead>
            <tbody>
                <tr role="row">
                    <?php echo $html;
                    ?>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                        <div class="col-sm-12">
                                <a href="{{ url('admin/blog/add')}}"> <button name="submit" type="submit" class="btn btn-success">ADD Blog</button></a>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
@endsection
