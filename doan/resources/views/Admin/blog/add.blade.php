@extends('Admin.layouts.app')
@section("content") 
    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
        {{ csrf_field() }}
        <div class="form-group">
            <label class="col-md-12">Title</label>
                <div class="col-md-12">
                    <input type="text" name="title" value="" class="form-control form-control-line">
                </div>
        </div>
        <div class="form-group">
            <label class="col-md-12">Image</label>
                 <input type="file" name="image" >
        </div>
        <div class="form-group">
            <label for="example-email" class="col-md-12">Description</label>
                <div class="col-md-12">
                    <textarea name="description" style="width: 100%;"></textarea>
                </div>
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea class="form-control" name="content" rows="5"></textarea>
        </div>
        <div class="form-group">
            @if($errors->any())
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif                           
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <button name="submit" type="submit" class="btn btn-success">ADD BLOG</button>
            </div>
        </div>
    </form>
@endsection

