@extends('frontend.layouts.app')
@section("content")
   <div class="col-sm-4 col-sm-offset-1">
        <div class="login-form"><!--login form-->
            <h2>Login to your account</h2>
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                     {{ csrf_field() }}
                    <input type="email" name="email">
                    <input type="password" name="password">
                    <span>
                         <input type="checkbox" class="checkbox"> 
                                Keep me signed in
                    </span>
                    <button type="submit" name="submit" class="btn btn-default">Login</button>
                </form>
                @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                @endif
        </div><!--/login form-->
    </div>
@endsection