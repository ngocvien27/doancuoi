@extends('frontend.layouts.app2')
@section("content")
                <div class="col-sm-4 col-sm-offset-1">
            <div class="signup-form"><!--sign up form-->
                        <h2>USER UPDATE</h2>
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                                      {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-12">Full Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="<?php echo auth::user()->name; ?>" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="email" readonly="" value="<?php echo auth::user()->email; ?>" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" name="password" value="<?php echo auth::user()->password; ?>" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" name="phone" value="<?php echo auth::user()->phone; ?>" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-12">
                                            <input name="address" type="text" value="<?php echo auth::user()->address; ?>" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="avatar" >
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Select Country</label>
                                        <div class="col-sm-12">
                                            <select name="country"  class="form-control form-control-line">
                                                @foreach($country as $key=>$value)
                                                {
                                                     <option value = "{{$value->id}}" {{$user->country == $value->id ? 'selected' : ''}}> {{$value->name}}</option> 
                                                } 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif                           
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button name="submit" type="submit" class="btn btn-success">Update</button>
                                        </div>
                                    </div>
                                </form>
                    </div><!--/sign up form-->
        </div>
@endsection
