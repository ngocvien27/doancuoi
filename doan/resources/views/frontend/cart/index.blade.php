@extends('frontend.layouts.app')
@section("content")
        <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active">Shopping Cart</li>
                </ol>
            </div>
            
            <?php 
                if(!Auth::check()){
            ?>
            <form action="{{ url('/cart/checkout')}}" id="register" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                                      {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-12">Full Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="email" value="" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Password</label>
                                        <div class="col-md-12">
                                            <input type="password" name="password" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" name="phone" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Address</label>
                                        <div class="col-md-12">
                                            <input name="address" type="text" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="avatar" >
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Select Country</label>
                                        <div class="col-sm-12">
                                            <select name="country"  class="form-control form-control-line">
                                                @foreach($country as $key=>$value)
                                                {
                                                     <option value = "{{$value->id}}"> {{$value->name}}</option> 
                                                } 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif                           
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button name="submit" type="submit" class="btn btn-success">Register</button>
                                        </div>
                                    </div>
                                </form>
                <?php
                    }
                ?>
            <input type="hidden" id="check" name="">
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description"></td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $value)
                            <?php
                                $tonghang=$value['qty']*$value['price'];
                                $tongcart=$tongcart+$tonghang; 
                            ?>
                        <tr>
                            <td class="cart_product">
                                <a href=""><img width="100px" height="100px" src="{{ asset('upload/user/product/'.$value['image']) }}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$value['name']}}</a></h4>
                                <p id="cart">{{$value['id']}}</p>
                            </td>
                            <td class="cart_price">
                                <p id="pri">{{$value['price']}}Đ</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <a class="cart_quantity_up" > + </a>
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
                                    <a class="cart_quantity_down"> - </a>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price"><?php echo $tonghang; ?>Đ</p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>
                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul class="user_option">
                            <li>
                                <input type="checkbox">
                                <label>Use Coupon Code</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Use Gift Voucher</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Estimate Shipping & Taxes</label>
                            </li>
                        </ul>
                        <ul class="user_info">
                            <li class="single_field">
                                <label>Country:</label>
                                <select>
                                    <option>United States</option>
                                    <option>Bangladesh</option>
                                    <option>UK</option>
                                    <option>India</option>
                                    <option>Pakistan</option>
                                    <option>Ucrane</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>
                                
                            </li>
                            <li class="single_field">
                                <label>Region / State:</label>
                                <select>
                                    <option>Select</option>
                                    <option>Dhaka</option>
                                    <option>London</option>
                                    <option>Dillih</option>
                                    <option>Lahore</option>
                                    <option>Alaska</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>
                            
                            </li>
                            <li class="single_field zip-field">
                                <label>Zip Code:</label>
                                <input type="text">
                            </li>
                        </ul>
                        <a class="btn btn-default update" href="">Get Quotes</a>
                        <a class="btn btn-default check_out" href="">Continue</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span>$59</span></li>
                            <li>Eco Tax <span>$2</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span id="tongcart"><?php echo $tongcart; ?></span></li>
                        </ul>
                           <?php 
                                if(Auth::check()){
                            ?>
                            <form action="{{ url('/cart/checkout')}}" method="post">
                                 {{ csrf_field() }}
                                    <input type="hidden" value="{{ Auth::user()->email }}" name="email">
                                    <input type="hidden" value="{{ Auth::user()->name }}" name="name">
                                    <input type="hidden" value="{{ Auth::user()->phone }}" name="phone">
                                    <input type="hidden" value="{{ Auth::user()->address }}" name="address">
                                    <button type="submit" class="btn btn-default check_out">Checkout</button>
                            </form>
                            <?php
                                }
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
@endsection
<link type="text/css" rel="stylesheet" href="{{ asset('rate/css/rate.css') }}">
    <script src="{{ asset('rate/js/jquery-1.9.1.min.js') }}"></script>
    <script>
        $(document).ready(function(){
        var tong=0;
        var total=$("span#tongcart").text()
        $('.cart_quantity_up').click(function(){
            var getqty=$(this).closest('tr').find('input.cart_quantity_input').val()        
            var id=$(this).closest("tr").find('p#cart').text()
            var pri=$(this).closest("tr").find('p#pri').text()
            getqty=parseInt(getqty)+1
            tong=parseInt(pri)*parseInt(getqty)
            $(this).closest("tr").find('input.cart_quantity_input').val(getqty)
            $(this).closest("tr").find('p.cart_total_price').text(tong)
            $.ajax({
                    method: "POST",// phương thức dữ liệu được truyền đi
                    url:"{{ route('ajaxQty.post') }}",// gọi đến file server show_data.php để xử lý
                    data: 
                    {
                        id: id,
                        qty: getqty,
                        _token: '{{csrf_token()}}',                      
                    },
                    success : function(response)
                    {
                        //kết quả trả về từ server nếu gửi thành công
                    }
            });  
            total=parseInt(total)+parseInt(pri);
            $("span#tongcart").text(total)                           
        })
         $('.cart_quantity_down').click(function(){
            var getqty=$(this).closest('tr').find('input.cart_quantity_input').val()        
            var id=$(this).closest("tr").find('p#cart').text()
            var pri=$(this).closest("tr").find('p#pri').text()
            getqty=parseInt(getqty)-1
            if(getqty>0)
                {   
                    tong=parseInt(pri)*parseInt(getqty)
                    $(this).closest("tr").find('input.cart_quantity_input').val(getqty)
                    $(this).closest("tr").find('p.cart_total_price').text(tong)                                                 
                }
            else
            {
                $(this).closest('tr').remove();
            }
            $.ajax({
                method: "POST",// phương thức dữ liệu được truyền đi
                url: "{{ route('ajaxQty.post') }}",// gọi đến file server show_data.php để xử lý
                data: 
                {
                    id: id,
                    qty: getqty,
                    _token: '{{csrf_token()}}',                  
                },
                success : function(response)
                {

                }
            });
            total=parseInt(total)-parseInt(pri)
            $("span#tongcart").text(total)                 
        })
         $('.cart_quantity_delete').click(function(){
                var id=$(this).closest("tr").find('p#cart').text()
                var pri=$(this).closest("tr").find('p#pri').text()
                var getqty=$(this).closest('tr').find('input.cart_quantity_input').val()             
                $(this).closest("tr").remove();
                total=parseInt(total)-(parseInt(pri)*parseInt(getqty))
                $("span#tongcart").text(total)
                getqty=0
                $.ajax({
                    method: "POST",// phương thức dữ liệu được truyền đi
                    url:"{{ route('ajaxQty.post') }}",// gọi đến file server show_data.php để xử lý
                    data: 
                    {
                        id: id,
                        qty: getqty,
                        _token: '{{csrf_token()}}',                     
                    },
                    success : function(response){//kết quả trả về từ server nếu gửi thành công
                    }
                });
            })
    });
</script>