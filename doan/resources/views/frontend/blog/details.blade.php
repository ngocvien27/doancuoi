@extends('frontend.layouts.app')
@section("content")
    <div class="col-sm-9">
                    <div class="blog-post-area">
                        <h2 class="title text-center">Latest From our Blog</h2>
                        <div class="single-blog-post"> 
                            <h3>{{$data->title}}</h3>
                            <div class="post-meta">
                                <ul>
                                    <li><i class="fa fa-user"></i> Mac Doe</li>
                                    <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                    <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                </ul>
                                <!-- <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-o"></i>
                                </span> -->
                            </div>
                            <a href="">
                                <img src="{{ asset('upload/user/blog/'.$data->image) }}" alt="">
                            </a>
                            {{$data->description}}
                            <p>
                              {{$data->content}}  
                            </p>     
                            <div class="pager-area">
                                <ul class="pager pull-right">
                                    @if($prev)
                                        <li><a href="{{ $prev }}">pre</a></li>
                                    @endif
                                    @if($next)
                                        <li><a href="{{ $next }}">next</a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div><!--/blog-post-area-->

                   <div class="rating-area">
                        <ul class="ratings">
                            <li class="rate-this">Rate this item:</li>
                            <li>
                                <div class="rate">
                                    @for($i=1;$i<=5;$i++)
                                        @if($i<=$avg)
                                            <div class="star_{{$i}} ratings_stars ratings_over"><input value="{{$i}}" type="hidden"></div>
                                        @endif
                                        @if($i>$avg)
                                           <div class="star_{{$i}} ratings_stars"><input value="{{$i}}" type="hidden"></div> 
                                        @endif
                                    @endfor
                                </div>
                            </li>
                            <li class="color">({{ $vote }} votes)</li>
                        </ul>
                        <ul class="tag">
                            <li>TAG:</li>
                            <li><a class="color" href="">Pink <span>/</span></a></li>
                            <li><a class="color" href="">T-Shirt <span>/</span></a></li>
                            <li><a class="color" href="">Girls</a></li>
                        </ul>
                    </div><!--/rating-area-->

                    <div class="socials-share">
                        <a href=""><img src="{{ asset('frontend/images/blog/socials.png') }}" alt=""></a>
                    </div><!--/socials-share-->

                    <!-- <div class="media commnets">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="images/blog/man-one.jpg" alt="">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">Annie Davis</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <div class="blog-socials">
                                <ul>
                                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                    <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                                <a class="btn btn-primary" href="">Other Posts</a>
                            </div>
                        </div>
                    </div> --><!--Comments-->
                    <div class="response-area">
                        <h2>{{$response}} RESPONSES</h2>
                        <ul class="media-list">
                        @foreach($data['comment'] as $value_cha)
                            @if($value_cha->id_cmt_cha==0)
                                <li><input type="hidden" class="id_cmt" value='{{$value_cha->id}}'></li>
                                <li class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="{{ asset('upload/user/avatar/'.$value_cha->avatar_user) }}" style="height:15%" alt="">
                                </a>
                                <div class="media-body">
                                    <ul class="sinlge-post-meta">
                                        <li><i class="fa fa-user"></i>{{ $value_cha->name_user }}</li>
                                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                        <input class="id" type="hidden" value="{{$data->id}}">
                                    </ul>
                                    <p>{{$value_cha->comment}}</p>
                                    <a class="btn btn-primary" id="rep_cha"><i class="fa fa-reply"></i>Replay</a>
                                </div>
                            </li>
                            @endif
                            @foreach($data['comment'] as $value_con)
                                @if($value_cha->id==$value_con->id_cmt_cha)
                                    <li class="media second-media">
                                        <a class="pull-left" >
                                            <img class="media-object" src="{{ asset('upload/user/avatar/'.$value_con->avatar_user) }}" style="height:15%" alt="">
                                        </a>
                                        <div class="media-body">
                                            <ul class="sinlge-post-meta">
                                                <li><i class="fa fa-user"></i>{{ $value_con->name_user }} </li>
                                                <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                                <li><i class="fa fa-calendar"></i>DEC 5, 2013</li>
                                            </ul>
                                            <p> {{$value_con->comment}} </p>
                                        </div>
                                    </li>
                                @endif
                            @endforeach
                        @endforeach   
                             <li hidden="" id="new" class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="{{ asset('frontend/images/blog/man-two.jpg') }}" alt="">
                                </a>
                                <div class="media-body">
                                    <ul class="sinlge-post-meta">
                                        <li><i class="fa fa-user"></i>Janis Gallagher</li>
                                        <li><i class="fa fa-clock-o"></i> 1:33 pm</li>
                                        <li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
                                    </ul>
                                    <p class="cmt"></p>
                                    <a class="btn btn-primary" href=""><i class="fa fa-reply"></i>Replay</a>
                                </div>
                            </li>
                        </ul>                   
                    </div><!--/Response-area-->
                    <div class="replay-box">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2>Leave a replay</h2>
                            </div>
                            <div class="col-sm-8">
                                <div class="text-area">
                                   <form action="{{url('/blog/comment')}}" method="post">
                                    {{ csrf_field() }}
                                        <div class="blank-arrow">
                                            <label>Your Comments</label>
                                        </div>
                                        <span>*</span>
                                        <input id="id_blog_reply" type="hidden" name="id_cmt_cha" value="0">
                                        <input name="id_blog" type="hidden" value="{{$data->id}}">
                                        <textarea name="comment" id="comment" rows="11"></textarea>         
                                        <button type="submit" name="submit" id="submit" class="btn btn-primary">Post comment</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!--/Repaly Box-->
                </div>
@endsection
    <link type="text/css" rel="stylesheet" href="{{ asset('rate/css/rate.css') }}">
    <script src="{{ asset('rate/js/jquery-1.9.1.min.js') }}"></script>
    <script>
    $(document).ready(function(){
        $.ajaxSetup({
        headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.ratings_stars').hover(
            // Handles the mouseover
            function() {
                $(this).prevAll().andSelf().addClass('ratings_hover');
                // $(this).nextAll().removeClass('ratings_vote'); 
            },
            function() {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
            }
        );
        // xử lý rate
        $('.ratings_stars').click(function(){
            var id_blog=$(this).closest('div.col-sm-9').find('input.id').val();
            var Values=$(this).find("input").val();
            @if(Auth::check()==false) 
            {
                alert("Vui lòng đăng nhập để đánh giá!");
            }
            @endif 
            @if(Auth::check()==true) 
            {
                alert("Cảm ơn bạn đã đánh giá "+Values+" sao");
                if ($(this).hasClass('ratings_over')) {
                    $('.ratings_stars').removeClass('ratings_over');
                    $(this).prevAll().andSelf().addClass('ratings_over');
                } else {
                    $(this).prevAll().andSelf().addClass('ratings_over');
                }
                $.ajax({
                    type:'POST',
                    url:"{{ route('ajaxRate.post') }}",
                    data:{
                        rate:Values,
                        id_blog:id_blog,
                        _token: '{{csrf_token()}}',
                    },
                });
            }
            @endif
        });
        // xử lý comment
        $('a#rep_cha').click(function(){
                var id_cmt_cha=$(this).closest("div.response-area").find('input.id_cmt').val();
                $(this).closest("div.col-sm-9").find('input#id_blog_reply').val(id_cmt_cha);
                alert("Hãy nhập bình luận trả lời bên dưới!");
        });
        $('#submit').click(function(){
           @if(Auth::check()==false) 
            {
                alert("Vui lòng đăng nhập để đánh giá!");
                return false;
            }
            @else
            {
                var cmt=$(this).closest("div.col-sm-9").find('textarea#comment').val();
                if(cmt=="")
                {
                    alert("Hãy nhập bình luận!");
                    return false;
                }
            }
            @endif
        });
    });
    </script>