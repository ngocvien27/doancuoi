@extends('frontend.layouts.app2')
@section("content")
            <div class="col-sm-4 col-sm-offset-1">
                <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="id"><h2>Id</h2></td>
                            <td class="name"><h2>Name</h2></td>
                            <td class="image"><h2>Image</h2></td>
                            <td class="price"><h2>Price</h2></td>
                            <td class="Action"><h2>Action</h2></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody> 
                    @foreach ($product as $value) 
                        <tr role="row">
                            <td>{{$value->id}}</td>
                            <td>{{$value['name']}}</td>
                            <td><img  width="100px" height="100px" src="{{ asset('upload/user/product/'.$value->image[0]) }}"></td>
                            <td>{{$value['price']-($value['price']*$value['sale']/100)}}</td>
                            <td><a class="glyphicon glyphicon-pencil" href="{{ url('/account/product/edit/'.$value['id']) }}"> Edit</a></td>
                            <td><a class="glyphicon glyphicon-trash" href="{{ url('/account/product/delete/'.$value['id']) }}"> Delete</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                     <tfoot>
                    <td colspan="8">
                        <a href="{{ url('/account/product/add') }}"><button class="btn btn-default" id="button">Create Product</button></a>
                    </td>
                </tr>
            </tfoot>
                </table>
            </div>
        </div>
@endsection
