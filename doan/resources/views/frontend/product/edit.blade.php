@extends('frontend.layouts.app2')
@section("content")
              <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                                      {{ csrf_field() }}
                                    @foreach($product as $product)
                                    <div class="form-group">
                                        <label class="col-md-12">Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="{{$product->name}}" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Price</label>
                                        <div class="col-md-12">
                                            <input type="text" name="price" value="{{$product->price}}" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Category</label>
                                        <div class="col-sm-12">
                                            <select name="id_category"  class="form-control form-control-line">
                                                @foreach($category as $key=>$value)
                                                {
                                                      <option value = "{{$value->id}}" {{$product->id_category == $value->id ? 'selected' : ''}}> {{$value->name}}</option> 
                                                } 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Brands</label>
                                        <div class="col-sm-12">
                                            <select name="id_brands"  class="form-control form-control-line">
                                              @foreach($brands as $key=>$value)
                                                {
                                                      <option value = "{{$value->id}}" {{$product->id_brands == $value->id ? 'selected' : ''}}> {{$value->name}}</option> 
                                                } 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Sale</label>
                                        <div class="col-sm-12">
                                            <select name="status" id="sale" class="form-control form-control-line" onchange="changeFunc(value);">
                                                @if($product->status ==0)   
                                                    <option value = "0" selected="selected">New</option>
                                                    <option value = "1">Sale</option>
                                                @else
                                                    <option value = "0">New</option>
                                                    <option value = "1" selected="selected" >Sale</option> 
                                                @endif      
                                                    <input type="{{$product->status==1 ? 'text' : 'hidden'}}" value="{{$product->sale}}" class="form-control form-control-line" id="sale" name="sale">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Company Profile</label>
                                        <div class="col-md-12">
                                            <input name="company" type="text" value="{{$product->company}}" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="image[]" multiple >
                                    </div>
                                    <div class="form-group">
                                        <table>
                                            <tr>
                                                @foreach($product->image as $value)                                 
                                                   <th> <img width="50px" height="50px" src="{{ asset('upload/user/product/'.$value)}}"></th>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                 @foreach($product->image as $value)
                                                    <th><input type="checkbox" name="   delete[]" value="{{$value}}"></th>
                                                @endforeach  
                                            </tr>                    
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Details</label>
                                        <div class="col-sm-12">
                                            <textarea name="details" type="text" value="{{$product->details}}" class="form-control form-control-line"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif                           
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button name="submit" type="submit" class="btn btn-success">Update Product</button>
                                        </div>
                                    </div>
                                    @endforeach
                                </form>
@endsection
 <link type="text/css" rel="stylesheet" href="{{ asset('rate/css/rate.css') }}">
    <script src="{{ asset('rate/js/jquery-1.9.1.min.js') }}"></script>
    <script>
        function changeFunc(i){
            if(i==1)
            {
                $('input#sale').attr('type','text');
            }
            else{
                $('input#sale').attr('type','hidden');
                $('input#sale').val(0);
            }
        }
    </script>