@extends('frontend.layouts.app2')
@section("content")
              <form action="" method="post" enctype="multipart/form-data" class="form-horizontal form-material">
                                      {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-12">Name</label>
                                        <div class="col-md-12">
                                            <input type="text" name="name" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Price</label>
                                        <div class="col-md-12">
                                            <input type="text" name="price" value="" class="form-control form-control-line" name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Category</label>
                                        <div class="col-sm-12">
                                            <select name="id_category"  class="form-control form-control-line">
                                                @foreach($category as $key=>$value)
                                                {
                                                     <option value = "{{$value->id}}"> {{$value->name}}</option> 
                                                } 
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Brands</label>
                                        <div class="col-sm-12">
                                            <select name="id_brands"  class="form-control form-control-line">
                                                @foreach($brands as $key=>$value)
                                                {
                                                     <option value = "{{$value->id}}"> {{$value->name}}</option> 
                                                }
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Sale</label>
                                        <div class="col-sm-12">
                                            <select name="status" id="sale" class="form-control form-control-line" onchange="changeFunc(value);">
                                                <option value = "0">New</option>
                                                <option value = "1">Sale</option>
                                            </select>
                                            <input type="hidden" value="0" id="sale" class="form-control form-control-line" name="sale">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Company Profile</label>
                                        <div class="col-md-12">
                                            <input name="company" type="text" value="" class="form-control form-control-line">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="image[]" multiple >
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-12">Details</label>
                                        <div class="col-sm-12">
                                            <textarea name="details" class="form-control form-control-line"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    @endif                           
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button name="submit" type="submit" class="btn btn-success">Add Product</button>
                                        </div>
                                    </div>
                                </form>
@endsection
 <link type="text/css" rel="stylesheet" href="{{ asset('rate/css/rate.css') }}">
    <script src="{{ asset('rate/js/jquery-1.9.1.min.js') }}"></script>
    <script>
        function changeFunc(i){
            if(i==1)
            {
                $('input#sale').attr('type','text');
            }
            else{
                $('input#sale').attr('type','hidden');
            }
        }
    </script>