<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        
        <style type="text/css">
        	div.demo {
        		width: 50px;
        		height: 50px;
        		display: inline-block;
        		background: blue;
        	}
        	table {
        		max-width: 100%;
    			background-color: transparent;
        	}
        	thead {
        		    display: table-header-group;
				    vertical-align: middle;
				    border-color: inherit;
        	}
        	tbody {
        			display: table-row-group;
				    vertical-align: middle;
				    border-color: inherit;
        	}
        </style>
    </head>
    <body>
       <div class="demo"></div>
        <p>Thông tin khách hàng:</p>
        <p>Full name:{{ Auth::user()->name }}</p>
        <p>Phone:{{ Auth::user()->phone }}</p>
        <p>Address:{{ Auth::user()->address }}</p>
    	<p></p>
    	<p></p>
    	<p>Thông tin giỏ hàng:</p>
        <table class="table table-condensed">
				<thead>
					<tr class="cart_menu" id="">
						<td class="description">name</td>
						<td class="price">Price</td>
						<td class="quantity">Quantity</td>
						<td class="total">Total</td>
						<td></td>
					</tr>
				</thead>
				 <tbody>
                        @foreach($product as $value)
                        <tr>
                            <td class="cart_description">
                                <h4><a href="">{{$value['name']}}</a></h4>
                                <p id="cart">{{$value['id']}}</p>
                            </td>
                            <td class="cart_price">
                                <p id="pri">{{$value['price']}}Đ</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <input class="cart_quantity_input" type="text" name="quantity" value="{{$value['qty']}}" autocomplete="off" size="2">
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">{{ $value['price']*$value['qty']}}</p>
                            </td>
                        </tr>
                        @endforeach
                        <div class="col-sm-6">
                    	<div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span>$59</span></li>
                            <li>Eco Tax <span>$2</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span id="tongcart">{{$sum}}</span></li>
                        </ul>
                    </div>
                </div>
				</tbody>
			</table>
    </body>
</html>