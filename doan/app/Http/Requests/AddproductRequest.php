<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddproductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          return [
            'name'=>'required',
            'price'=>'required',
            'company'=>'required',
            'details'=>'required',
            //  'image' => [
            //     'mimes:jpeg,jpg,png,gif',
            //     'max:2048',
            // ],  
        ];
    }
    public function messages()
    {
        return [
            'required'=>'Hãy nhập :attribute',
            'max'=>':attribute quá lớn',
            'mimes'=>'chỉ được chọn ảnh',
        ];
    }
     public function attributes(){
        return [
            'name'=>'tên',
            'image'=>'ảnh',
            'price'=>'price',
            'company'=>'company',
            'details'=>'details',
        ];
    }
}
