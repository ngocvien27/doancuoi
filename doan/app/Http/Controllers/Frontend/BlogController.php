<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use App\users;
use App\rate;
use App\Comment;
use Auth;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Blog::orderBy('id', 'desc')->paginate(3);
        return view('frontend/blog/list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {      
        //xử lý phân trang
        $prev = Blog::where('id', '>', $id)->min('id');
        $next = Blog::where('id', '<', $id)->max('id');
        // xử lý rate blog
        $rate=rate::select('rate')->where('id_blog','like',$id)->get();
        $sum=0;
        $vote=0;
        $avg=0;
        foreach ($rate as $value) {
            $sum+=$value['rate'];
            $vote++;
        }
        if ($vote==0) {
           $avg=0;
        }
        else{
            $avg=round($sum/$vote);
        }
         // xử lý comment blog
        $data=Blog::with(['comment'=>function($q){
            $q->orderBy('id','desc');
        }])->find($id);
        $response=0;
        foreach ($data['comment'] as $value) {
            $response++;
        }
        return view('frontend/blog/details',compact('next','prev','vote','avg','data','response'));
    }
     /**
     * Store a newly created resource in table rate.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxRatePost(Request $request)
    {
        $rate = $request->all();
        $rate['id_users']=Auth::id();
        if(rate::create($rate)){
            return response()->json(['success'=>"Đánh giá thành công!!!"]); 
        }else{
             return response()->json(['error'=>"Đánh giá thất bại!!!"]);
        }
    }
     /**
     * Store a newly created resource in table commnent.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxCommentPost(Request $request)
    {
        $comment = $request->all();
        $comment['id_users']=Auth::id();
        $comment['name_user']=Auth::user()->name;
        $comment['avatar_user']=Auth::user()->avatar;
        if(Comment::create($comment)){
             return redirect()->back()->with('success',__('bình luận thành công!!!'));
        }else{
            return redirect()->back()->withErrors('bình luận không thành công!!!');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
