<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Brands;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category=Category::all();
        $brands=Brands::all();
        $product=Product::orderBy('id', 'desc')->paginate(6);
        for ($i=0; $i <count($product) ; $i++) { 
            $product[$i]['image']=json_decode($product[$i]['image']);
        }
        return view('frontend/index',compact('product','category','brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $get_name=$_GET['search'];
        $get_price=$_GET['price'];
        $get_category=$_GET['id_category'];
        $get_brands=$_GET['id_brands'];
        $get_status=$_GET['status'];
        $product=Product::query();
        if($_GET['search']!="")
        {
            $product->where('name','LIKE','%'.$get_name.'%')->get();
        }
        if($_GET['price']!="")
        {
            if($_GET['price']=="500000")
            {
                $product->whereBetween('price',[0,500000])->get();
            }
            else if($_GET['price']=="1000000")
            {
                $product->whereBetween('price',[500001,1000000])->get();
            }
            else if($_GET['price']=="2000000")
            {
                $product->whereBetween('price',[1000001,2000000])->get();
            }
        }
        if($_GET['id_category']!="")
        {
            $product->where('id_category',$get_category)->get();
        }
        if($_GET['id_brands']!="")
        {
            $product->where('id_brands',$get_brands)->get();
        }
        if($_GET['status']!="")
        {
            $product->where('status',$get_status)->get();
        }
        else{
            $product->orderBy('id', 'desc')->paginate(6);
        }
        $product=$product->get();
        for ($i=0; $i <count($product) ; $i++) { 
            $product[$i]['image']=json_decode($product[$i]['image']);
        }
        $category=Category::all();
        $brands=Brands::all();
        return view('frontend/index',compact('product','category','brands'));
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxprice()
    {
        $price=$_POST['price'];
        $tach=(explode(',', $price));
        $product=Product::whereBetween('price',[$tach[0],$tach[1]])->get();
        for($i=0; $i<count($product);$i++){
            $product[$i]['image']=json_decode($product[$i]['image']);
        }
        return response()->json(['success'=>"thành công",'product'=>$product]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
