<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AddproductRequest;
use App\Doan;
use App\Product;
use App\Category;
use App\Brands;
use Image;
use Illuminate\Support\Facades\Auth; 
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product=Product::where('id_user',Auth::id())->get();
        for ($i=0; $i <count($product) ; $i++) { 
            $product[$i]['image']=json_decode($product[$i]['image']);
        }
        return view('/frontend/product/index',compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=Category::all();
        $brands=Brands::all();
        $product=Product::all();
        return view('/frontend/product/add',compact('category','brands','product'));
    }
    public function handlerCart($cart)
    {
        if($cart)
            {
                $time=strtotime(date('Y-m-d H:i:s'));
                foreach($cart as $image)
                {
                    $name =$time."_".$image->getClientOriginalName();
                    $name_2 = "Hinh85_".$time."_".$image->getClientOriginalName();
                    $name_3 = "Hinh329_".$time."_".$image->getClientOriginalName();

                    //$image->move('upload/product/', $name);
                
                    $path = public_path('upload/user/product/' . $name);
                    $path2 = public_path('upload/user/product/' . $name_2);
                    $path3 = public_path('upload/user/product/' . $name_3);

                    Image::make($image->getRealPath())->save($path);
                    Image::make($image->getRealPath())->resize(85, 84)->save($path2);
                    Image::make($image->getRealPath())->resize(329, 380)->save($path3);
                }
            }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddproductRequest $request)
    {
        if (count($request->image)<=3) 
        {
            $this->handlerCart($request->file('image'));
            $product=$request->all();
            $product['id_user']= Auth::id();
            foreach($request->file('image') as $value)
            {
                $value=$value->getClientOriginalName();
                $data[]=$value;
            }
            $product['image']=json_encode($data);
            if(Product::create($product)){
                return redirect('/account/product/index')->with('success',__('thêm thành công!!!'));
            }
        }
        else{
            return redirect()->back()->with('error',__('không up quá 3 hình')); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::where('id',$id)->get();
        $category=Category::all();
        $brands=Brands::all();
        $product[0]['image']=json_decode($product[0]['image']);
        return view('/frontend/product/edit',compact('product','category','brands'));    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AddproductRequest $request, $id)
    {
        $data=[];
        $product=Product::where('id',$id)->get();
        $product=json_decode($product[0]['image']);
        if(!empty($request->delete))
        {
            for($i=0;$i<=count($product);$i++){
                if(in_array($product[$i],$request->delete)){
                    unset($product[$i]); 
                }
            }
        }
        $this->handlerCart($request->file('image'));
        foreach($request->file('image') as $value)
        {
            $value=$value->getClientOriginalName();
            $data[]=$value;
        }
        $update_image=array_merge($product,$data); 
        if(count($update_image)<=3)
        {  
            $update=$request->all();
            $update['image']=json_encode($update_image);
            if(Product::find($id)->update($update))
            {
                return redirect('account/product/index')->with('success',__('update thành công!!!'));
            }
            else
            {
                return redirect()->back()->with('error',__('lỗi')); 
            }
        }
        else{
                return redirect()->back()->with('error',__('không up quá 3 hình')); 
            }
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        return redirect()->back()->with('success','Delete susscess');
    }
}
