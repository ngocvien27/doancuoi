<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\user;
use App\history;
use App\Country;
use App\Doan;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestMail;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        session_start();
        $data=[];
        if(isset($_SESSION["b"]))
        {
          $data=$_SESSION["b"];   
        }
        $tongcart=0;
        $country=Country::all();
        return view('/frontend/cart/index',compact('data','tongcart','country'));
    }
   /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxAddPost(Request $request)
    {
        session_start();
        $ajax_id=$_POST['id'];
        $data=Product::where('id',$ajax_id)->get();
        $data[0]['image']=json_decode($data[0]['image']);
        $demo=1;
        $id=$data[0]["id"];
        $image=$data[0]["image"][0];
        $name=$data[0]["name"]; 
        $price=$data[0]["price"];       
        $qty=1; 
        if(isset($_SESSION["b"]))
                {
                    $cart=$_SESSION["b"];
                    foreach ($cart as $key =>$value)
                    {
                        if($value["id"]==$id)
                        {       
                            $_SESSION["b"]["$key"]["qty"]++;    
                            $demo=2;        
                        }
                    }
                }
        if ($demo==1) 
        {
            $p=array("id"=>$id,"image"=>$image,"name"=>$name,"price"=>$price,"qty"=>$qty);
            $_SESSION["b"][]=$p; 
        }     
    }
    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ajaxQtyPost(Request $request)
    {
        session_start();
        $cart=$_SESSION["b"];
        foreach ($cart as $key =>$value)
        {
            if($value["id"]==$_POST['id'])
            {       
                $_SESSION["b"]["$key"]["qty"]=$_POST['qty'];        
                if($_SESSION["b"]["$key"]["qty"]==0)
                {
                    unset($_SESSION["b"]["$key"]);
                }
            }
        }               
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        session_start();

        if(!Auth::check())
        {
            $data=$request->all();     
            $data['level']=0;
            $data['password']=bcrypt($data['password']);
            $file=$request->file('avatar');
            if(!empty($file))
            {
                $data['avatar']=$file->getClientOriginalName();
            }
            if(Doan::create($data))
            {
                if(!empty($file))
                {
                    $file->move('upload/user/avatar',$file->getClientOriginalName());
                }
                $login=[
                    'email'=>$request->email,
                    'password'=>$request->password,
                    'level'=>0
                ];
                Auth::attempt($login);
                $this->sendmail();
            }
        }
        if(Auth::check())
        {
            $this->sendmail();
        }
        return redirect()->back()->with('success',__('Checkout thành công!!!'));    
    }
 /**
     * 
     * @return Illuminate\Support\Facades\Mail;
     */    
    public function sendmail()
    {
        $product=[];
        if(isset($_SESSION["b"]))
        {
          $product=$_SESSION["b"];   
        }
        $sum = 0;
        foreach ($product as $value) {
            $sum = $sum+$value['price']*$value['qty'];
        }
        // gửi email
        $emailTo = Auth::user()->email;
        $subject = "Mail order product";
        Mail::send('frontend.Email.TestMail', 
            array(
                'product'=>$product,
                'sum' => $sum
            ),

            function ($message) use ($subject, $emailTo){
                   $message->from('huynhngocvien27@gmail.com', 'Mail order product');
                   $message->to($emailTo);
                   $message->subject($subject);
        });
        // Thêm vào History
        $cart=[];
        $id=Auth::id();
        $user=user::where('id',$id)->get();
        $cart['id_user']=$id;
        $cart['email']=$user[0]["email"];
        $cart['phone']=$user[0]["phone"];
        $cart['name']=$user[0]["name"];
        $cart['price']=$sum;
        history::create($cart); 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
