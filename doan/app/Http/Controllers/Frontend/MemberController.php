<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MemberLoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Country;
use App\Doan;
use Illuminate\Support\Facades\Auth; 

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country=Country::all();
        return view('frontend/member/register',compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        $data=$request->all();     
        var_dump($data);
    }
    /**
     * Show the form for login.
     *
     * @return \Illuminate\Http\Response
     */
    public function form_login()
    {
       return view('frontend/member/login');
    }
    /**
     * Login from resources in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(MemberLoginRequest $request)
    {
       $login=[
            'email'=>$request->email,
            'password'=>$request->password,
            'level'=>0
        ];
        $remember=false;
        if($request->remember_me)
        {
            $remeber=true;
        }
        if(Auth::attempt($login,$remember)){
            return redirect('/index');
        }
        else{
            return redirect()->back()->withErrors('Email Or Password is not correct');
        }
    }
    /**
     * Show the form for login.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/member/login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
