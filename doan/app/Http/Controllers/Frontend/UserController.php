<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateRequest;
use App\Country;
use App\Doan;
use Illuminate\Support\Facades\Auth; 

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $country=Country::all();
        $user=auth::user();
        return view('frontend/member/account',compact('country','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdateRequest $request)
    {
        $id=auth::id();
        $user=Doan::find($id);
        $data=$request->all();
        $file=$request->file('avatar');
            if(!empty($file))
            {
                $data['avatar']=$file->getClientOriginalName();
            }
            if($data['password'])
            {
                $data['password']=bcrypt($data['password']);
            }
            else
            {
                $data['password']=$user->password;
            }
            if($user->update($data))
            {
                if(!empty($file))
                {
                    $file->move('upload/user/avatar',$file->getClientOriginalName());
                }
                return redirect()->back()->with('success',__('update profile success')); 
            }
            else
            {
                return redirect()->back()->withErrors('update profile error');
            }        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
