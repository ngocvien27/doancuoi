<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doan extends Model
{
    protected $table="users";
    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'phone',
        'address',
        'country',
        'avatar',
        'remember_token',
        'level'
    ];
    public $timestamps=false;   
}
