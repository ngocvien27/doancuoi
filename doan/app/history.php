<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class history extends Model
{
    protected $table="history";
    protected $fillable = [
        'email','phone','name','id_user','price','created_at','updated_at',
    ];
}
