<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table="comment";
    protected $fillable = ['id','id_users','id_blog','comment','name_user','avatar_user','id_cmt_cha'];
    public $timestamps=false;
}
