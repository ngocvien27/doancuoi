<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table="product";
    protected $fillable = ['id','name', 'image','price','id_user','id_category','id_brands','status','sale','company','details',
       
    ];
     public $timestamps=false;  
}
