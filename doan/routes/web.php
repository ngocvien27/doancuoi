<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Frontend
Route::group([
    'namespace'=>'Frontend'
],function(){

// Login
Route::get('/member/login','MemberController@form_login');
Route::post('/member/login','MemberController@login');

// Register
Route::get('/member/register','MemberController@create');
Route::post('/member/register','MemberController@store');

// logout
 Route::get('/member/logout', 'MemberController@logout');

// index
Route::get('/index','HomeController@index');

// ---------------------------blog
// Blog List
Route::get('/blog/list','BlogController@index');

// Blog Details
Route::get('/blog/details/{id}','BlogController@show');

// rate Blog
Route::post('ajaxRate', 'BlogController@ajaxRatePost')->name('ajaxRate.post');

// comment Blog
Route::post('/blog/comment','BlogController@ajaxCommentPost')->name('ajaxComment.post');
// ---------------------------

// ---------------------------Account
//##### Account Update
Route::get('/account/update','UserController@index');
Route::post('/account/update','UserController@store');
//##### Product
// Index
Route::get('/account/product/index','ProductController@index');
Route::post('/account/product/index','ProductController@store');

// Add
Route::get('/account/product/add','ProductController@create');
Route::post('/account/product/add','ProductController@store');

// Delete
Route::get('/account/product/delete/{id}','ProductController@destroy');
//edit
Route::get('/account/product/edit/{id}','ProductController@edit');
Route::post('/account/product/edit/{id}','ProductController@update');
// ---------------------------

// ---------------------------Cart
//Index 
Route::get('/cart/index','CartController@index');

// Add 
Route::post('/cart/add','CartController@ajaxAddPost')->name('ajaxAdd.post');

// up_down cart
Route::post('/cart/up_donwn_qty','CartController@ajaxQtyPost')->name('ajaxQty.post');

// register and login for checkout
Route::post('/cart/checkout','CartController@checkout');

// search product
Route::get('/search','HomeController@search');
// Search price
Route::post('/search/price','HomeController@ajaxPrice')->name('ajaxPrice.post');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/auth/login', 'Auth\LoginController@logout');

// Admin
Route::group([
    'prefix'=>'admin',
    'namespace'=>'admin'
],function(){

//dashboard
Route::get('/dashboard/index', 'DashboardController@index');

//update user
Route::get('/user/profile', 'UserController@index');
Route::post('/user/profile',"UserController@store");

//index country
Route::get('/country/country', 'CountryController@index');

//create country
Route::get('/country/add', 'CountryController@create');
Route::post('/country/add',"CountryController@store");

// delete country
Route::get('/country/delete/{id}', 'CountryController@destroy');

// index blog
Route::get('/blog/blog', 'BlogController@index');

// create blog
Route::get('/blog/add', 'BlogController@create');
Route::post('/blog/add',"BlogController@store");

// edit blog
Route::get('/blog/edit/{id}', 'BlogController@edit');
Route::post('/blog/edit/{id}',"BlogController@update");

// delete blog
Route::get('/blog/delete/{id}', 'BlogController@destroy'); 
});




